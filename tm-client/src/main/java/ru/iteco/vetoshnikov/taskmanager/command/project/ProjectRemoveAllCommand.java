package ru.iteco.vetoshnikov.taskmanager.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-removeall";
    }

    @Override
    public String description() {
        return "удаляет все ваши проекты.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getProjectEndpointService().getProjectEndpointPort().removeAllByUserProject(session.getUserId());
    }
}
