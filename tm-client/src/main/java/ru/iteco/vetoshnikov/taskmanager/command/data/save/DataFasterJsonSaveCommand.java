package ru.iteco.vetoshnikov.taskmanager.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataFasterJsonSaveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-save-fjson";
    }

    @Override
    public String description() {
        return "сохраняет базу в *.Json файл.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().saveFasterJson(session);
        System.out.println("Выгрузка базы завершена");
    }
}
