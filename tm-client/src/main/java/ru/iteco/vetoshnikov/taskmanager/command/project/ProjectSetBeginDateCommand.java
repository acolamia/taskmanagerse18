package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.util.DateUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor
public final class ProjectSetBeginDateCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "project-change-begindate";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет время начала проекта.";
    }

    @Override
    public void execute() throws DatatypeConfigurationException {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Введите имя проекта для которого нужно изменить время начала: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        System.out.println("Введите дату начала проекта в формате ДД.ММ.ГГГГ: ");
        @Nullable final String newBeginDateString = service.getScanner().nextLine();
        @Nullable final Date newDate = DateUtil.parseDate(newBeginDateString);
        @Nullable final ProjectDTO project = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findOneProject(session.getUserId(),projectName);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(newDate);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        project.setBeginDate(xmlGregorianCalendar);

        project.setBeginDate(xmlGregorianCalendar);
        serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(project);
    }
}
