package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-29T16:56:55.147+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/ISessionEndpoint/getSessionRequest", output = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/ISessionEndpoint/getSessionResponse")
    @RequestWrapper(localName = "getSession", targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", className = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.GetSession")
    @ResponseWrapper(localName = "getSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", className = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.GetSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO getSession(
        @WebParam(name = "userLogin", targetNamespace = "")
        java.lang.String userLogin,
        @WebParam(name = "userPass", targetNamespace = "")
        java.lang.String userPass
    );

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/ISessionEndpoint/getPortRequest", output = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/ISessionEndpoint/getPortResponse")
    @RequestWrapper(localName = "getPort", targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", className = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.GetPort")
    @ResponseWrapper(localName = "getPortResponse", targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", className = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.GetPortResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getPort();

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/ISessionEndpoint/getUrlRequest", output = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/ISessionEndpoint/getUrlResponse")
    @RequestWrapper(localName = "getUrl", targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", className = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.GetUrl")
    @ResponseWrapper(localName = "getUrlResponse", targetNamespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", className = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.GetUrlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getUrl();
}
