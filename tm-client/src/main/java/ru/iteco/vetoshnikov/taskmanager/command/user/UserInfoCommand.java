package ru.iteco.vetoshnikov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserInfoCommand extends AbstractCommand {
    @Override
    public String command() {
        return "info";
    }

    @Override
    public String description() {
        return "Отображение информации о текущем пользователе.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("ID пользователя: " + session.getUserId());
    }
}
