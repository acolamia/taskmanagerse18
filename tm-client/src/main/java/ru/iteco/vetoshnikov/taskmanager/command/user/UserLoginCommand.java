package ru.iteco.vetoshnikov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.Nullable;

public final class UserLoginCommand extends AbstractCommand {
    public UserLoginCommand() {
        super();
        setSecure(false);
    }

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "Вызвать авторизацию пользователя.";
    }

    @Override
    public void execute() {
        System.out.print("Login: ");
        @Nullable final String login = service.getScanner().nextLine();
        System.out.print("Password: ");
        @Nullable final String password = service.getScanner().nextLine();

        @NotNull final SessionDTO session = serviceLocator.getSessionEndpointService().getSessionEndpointPort().getSession(login, password);
        if (session == null) {
            System.out.println("Повторите попытку авторизации.");
            return;
        }

        serviceLocator.getSessionStatusService().setSession(session);
        System.out.println("Авторизация прошла успешно");
    }
}
