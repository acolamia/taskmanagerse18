package ru.iteco.vetoshnikov.taskmanager.api.customApi;

public interface ISessionStatusService {
    ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO getSession();

    void setSession(ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO session);
}
