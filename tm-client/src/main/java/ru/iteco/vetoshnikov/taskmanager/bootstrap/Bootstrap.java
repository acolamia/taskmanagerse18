package ru.iteco.vetoshnikov.taskmanager.bootstrap;

import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.IService;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.customApi.*;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.ICommandService;
import ru.iteco.vetoshnikov.taskmanager.service.SessionStatusService;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@Component
public class Bootstrap implements IServiceLocator, IService {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SessionStatusService sessionStatusService;

    @Autowired
    private IDomainEndpointService domainEndpointService;

    @Autowired
    private IProjectEndpointService projectEndpointService;

    @Autowired
    private ITaskEndpointService taskEndpointService;

    @Autowired
    private IUserEndpointService userEndpointService;

    @Autowired
    private ISessionEndpointService sessionEndpointService;

    @Autowired
    private ICommandService commandService;

    private Scanner sc = new Scanner(System.in);

    private Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.iteco.vetoshnikov.taskmanager").getSubTypesOf(AbstractCommand.class);

    public void init() {
        try {
            registerCommandClasses(classes);
            System.out.println("Taskmanager");
            System.out.println("Введите help чтобы получить справку по командам ");
            while (true) {
                @Nullable final String enterCommand = sc.nextLine();
                if (!commandService.getCommandMap().containsKey(enterCommand)) {
                    System.out.println("Такой команды не существует.");
                    continue;
                }
                if (commandService.getCommandMap().get(enterCommand).isSecure() && sessionStatusService.getSession() == null) {
                    System.out.println("Необходима авторизация.");
                    continue;
                }
                commandService.getCommandMap().get(enterCommand).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerCommandClasses(@Nullable Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null) return;
        for (@Nullable Class getClass : classes) {
            if (getClass == null || !AbstractCommand.class.isAssignableFrom(getClass)) {
                continue;
            }
            @NotNull AbstractCommand command = (AbstractCommand) getClass.newInstance();
            command.setServiceLocator(this);
            command.setService(this);
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandService.getCommandMap().put(command.command(), command);
    }

    @SneakyThrows
    @Override
    public Scanner getScanner() {
        return sc;
    }
}
