package ru.iteco.vetoshnikov.taskmanager.command.about;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

import javax.xml.datatype.DatatypeConfigurationException;

public class ServerInfoCommand extends AbstractCommand {
    @Override
    public String command() {
        return "server-info";
    }

    @Override
    public String description() {
        return "Информация о сервере.";
    }

    @Override
    public void execute() throws DatatypeConfigurationException {
        System.out.println("SERVER INFO");
        System.out.println(serviceLocator.getSessionEndpointService().getSessionEndpointPort().getUrl());
        System.out.println(serviceLocator.getSessionEndpointService().getSessionEndpointPort().getPort());
        System.out.println("SERVER STATUS:");
        System.out.println("OK");
    }
}
