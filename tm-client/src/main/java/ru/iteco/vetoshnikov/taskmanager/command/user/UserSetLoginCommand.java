package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.UserDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserSetLoginCommand extends AbstractCommand {
    @Override
    public String command() {
        return "set-login";
    }

    @Override
    public String description() {
        return "Изменение логина пользователя.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите логин пользователя для которого хотите изменить логин: ");
        @Nullable final String login = service.getScanner().nextLine();
        System.out.print("Введите новый логин: ");
        @Nullable final String newLogin = service.getScanner().nextLine();
        @Nullable final UserDTO user = serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(login);
        user.setLogin(newLogin);
        serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(user);
    }
}
