package ru.iteco.vetoshnikov.taskmanager.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataJaxbJsonLoadCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-load-jaxbj";
    }

    @Override
    public String description() {
        return "загружает jabxJson.json файл в базу.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().loadJaxbJson(session);
        System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
    }
}
