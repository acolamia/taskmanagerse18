package ru.iteco.vetoshnikov.taskmanager.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TaskSearchByWordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-search";
    }

    @Override
    public String description() {
        return "поиск по названию задачи.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        @NotNull final String userId = session.getUserId();
        if (userId == null) return;
        System.out.println("Введите название задачи:");
        @Nullable final String word = service.getScanner().nextLine();
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllTask();
        @Nullable final List<TaskDTO> taskListUser = new ArrayList<>();
        for (@Nullable final TaskDTO task: taskList){
            if (userId.equals(task.getUserId())){
                taskListUser.add(task);
            }
        }
        for (@Nullable final TaskDTO task : taskListUser) {
            if (task == null) continue;
            if (word.equalsIgnoreCase(task.getName())) System.out.println(task.getName());
        }
    }
}
