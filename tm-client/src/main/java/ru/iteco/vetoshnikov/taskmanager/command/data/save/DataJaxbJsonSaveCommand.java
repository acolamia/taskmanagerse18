package ru.iteco.vetoshnikov.taskmanager.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataJaxbJsonSaveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-save-jaxbj";
    }

    @Override
    public String description() {
        return "сохраняет в базу jaxbJson.json.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().saveJaxbJson(session);
        System.out.println("Выгрузка базы завершена");
    }
}
