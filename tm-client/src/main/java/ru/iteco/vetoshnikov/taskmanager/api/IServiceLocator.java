package ru.iteco.vetoshnikov.taskmanager.api;


import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.customApi.*;
import ru.iteco.vetoshnikov.taskmanager.api.service.ICommandService;

@Component
public interface IServiceLocator {

    ISessionStatusService getSessionStatusService();

    IProjectEndpointService getProjectEndpointService();

    ITaskEndpointService getTaskEndpointService();

    IUserEndpointService getUserEndpointService();

    IDomainEndpointService getDomainEndpointService();

    ISessionEndpointService getSessionEndpointService();

    ICommandService getCommandService();
}
