package ru.iteco.vetoshnikov.taskmanager.api.customApi;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ISessionEndpoint;

import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceFeature;

public interface ISessionEndpointService {
    @WebEndpoint(name = "SessionEndpointPort")
    ISessionEndpoint getSessionEndpointPort();

    @WebEndpoint(name = "SessionEndpointPort")
    ISessionEndpoint getSessionEndpointPort(WebServiceFeature... features);
}
