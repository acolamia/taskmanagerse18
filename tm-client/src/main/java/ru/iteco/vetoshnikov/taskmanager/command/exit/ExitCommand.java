package ru.iteco.vetoshnikov.taskmanager.command.exit;

import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {
    public ExitCommand() {
        super();
        setSecure(false);
    }

    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String description() {
        return "Команда выхода из приложения.";
    }

    @Override
    public void execute() {
        System.out.println("Программа закрыта.");
        service.getScanner().close();
        System.exit(0);
    }
}
