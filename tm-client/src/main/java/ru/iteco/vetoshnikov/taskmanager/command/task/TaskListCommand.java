package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "отображает список задач в веденном вами проекте.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите имя проекта: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session.getUserId(), projectName);
        @NotNull final List<TaskDTO> taskList = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllByProjectTask(session.getUserId(), projectId);
        System.out.println("Список задач:");
        for (@Nullable TaskDTO task : taskList) {
            System.out.println(task.getName());
        }
    }
}
