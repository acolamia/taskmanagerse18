
package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loadTask complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loadTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="domainObject" type="{http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/}domainDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loadTask", propOrder = {
    "domainObject"
})
public class LoadTask {

    protected DomainDTO domainObject;

    /**
     * Gets the value of the domainObject property.
     * 
     * @return
     *     possible object is
     *     {@link DomainDTO }
     *     
     */
    public DomainDTO getDomainObject() {
        return domainObject;
    }

    /**
     * Sets the value of the domainObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link DomainDTO }
     *     
     */
    public void setDomainObject(DomainDTO value) {
        this.domainObject = value;
    }

}
