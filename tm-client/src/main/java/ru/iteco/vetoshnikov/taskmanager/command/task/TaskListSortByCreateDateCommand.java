package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@NoArgsConstructor
public final class TaskListSortByCreateDateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list-sortbycreate";
    }

    @Override
    public String description() {
        return "отображает список задач, сортируя по дате создания.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Введите имя проекта, для которого нужно произвести сортировку:");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session.getUserId(), projectName);
        System.out.println("Список проектов сотрированный по дате создания:");
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllByProjectTask(session.getUserId(),projectId);
        @Nullable final Task[] tasks = taskList.toArray(new Task[0]);
        Arrays.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                if (o1.getCreateDate() == null && o2.getCreateDate() == null) return 0;
                if (o1.getCreateDate() != null && o2.getCreateDate() == null) return 1;
                if (o1.getCreateDate() == null && o2.getCreateDate() != null) return -1;
                return o1.getCreateDate().compare(o2.getCreateDate());
            }
        });
        @Nullable final List<Task> arrayList = Arrays.asList(tasks);
        for (@Nullable final Task task : arrayList) {
            if (task.getCreateDate() == null) {
                System.out.println(task.getName() + " - дата не указана.");
            }
            System.out.println(task.getName() + " - " + task.getCreateDate());
        }
    }
}
