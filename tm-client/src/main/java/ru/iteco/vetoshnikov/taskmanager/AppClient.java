package ru.iteco.vetoshnikov.taskmanager;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.iteco.vetoshnikov.taskmanager.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@ComponentScan
public final class AppClient {

    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(AppClient.class);
        @NotNull Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }
}