package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

@NoArgsConstructor
public final class TaskSetStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "task-change-status";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет статус задачи.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        @Nullable final String userId = session.getUserId();
        System.out.println("Введите имя задачи для которой нужно изменить статус: ");
        @Nullable final String taskName = service.getScanner().nextLine();
        System.out.println("В каком проекте находится эта задача: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session.getUserId(), projectName);
        System.out.println("Введите статус проекта из списка:");
        for (@Nullable StatusType statusType : StatusType.values()) {
            System.out.println(statusType);
        }
        @Nullable final String newSetStatus = service.getScanner().nextLine();
        @Nullable final TaskDTO task = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findOneTask(session.getUserId(), projectId, taskName);
        if (newSetStatus.equalsIgnoreCase("Запланировано")) {
            task.setStatusType(StatusType.PLANNED.getDisplayName());
            serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(task);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("В процессе")) {
            task.setStatusType(StatusType.INPROGRESS.getDisplayName());
            serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(task);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("Выполнено")) {
            task.setStatusType(StatusType.COMPLETE.getDisplayName());
            serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(task);
            return;
        }
    }
}
