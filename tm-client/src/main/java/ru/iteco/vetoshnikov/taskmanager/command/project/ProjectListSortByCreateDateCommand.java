package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@NoArgsConstructor
public final class ProjectListSortByCreateDateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list-sortbycreate";
    }

    @Override
    public String description() {
        return "отображает список проектов, сортируя по дате создания.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Список проектов сотрированный по дате создания:");
        @Nullable final List<ProjectDTO> projectList = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllByUserProject(session.getUserId());
        @Nullable final Project[] projects = projectList.toArray(new Project[0]);
        Arrays.sort(projects, new Comparator<Project>() {
            @Override
            public int compare(@Nullable final Project o1, @Nullable final Project o2) {
                if (o1.getCreateDate() == null && o2.getCreateDate() == null) return 0;
                if (o1.getCreateDate() != null && o2.getCreateDate() == null) return 1;
                if (o1.getCreateDate() == null && o2.getCreateDate() != null) return -1;
                return o1.getCreateDate().compare(o2.getCreateDate());
            }
        });
        @Nullable final List<Project> arrayList = Arrays.asList(projects);
        for (@Nullable final Project project : arrayList) {
            if (project.getCreateDate() == null) {
                System.out.println(project.getName() + " - дата не указана.");
            }
            System.out.println(project.getName() + " - " + project.getCreateDate());
        }

    }
}
