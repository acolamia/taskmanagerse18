package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "создается проект.";
    }

    @Override
    public void execute() {
        System.out.println("Введите желаемое имя для нового проекта: ");
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @Nullable final ProjectDTO project = new ProjectDTO();
        project.setName(projectName);
        project.setUserId(session.getUserId());
        projectEndpoint.createProjectProject(project);
    }
}
