package ru.iteco.vetoshnikov.taskmanager.command.project;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "отображает список проектов.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Список проектов:");
        @NotNull final List<ProjectDTO> projectList = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllByUserProject(session.getUserId());
        for (@Nullable final ProjectDTO project : projectList) {
            System.out.println(project.getName());
        }
    }
}
