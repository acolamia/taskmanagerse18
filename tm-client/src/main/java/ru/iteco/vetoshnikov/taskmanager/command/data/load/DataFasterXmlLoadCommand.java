package ru.iteco.vetoshnikov.taskmanager.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataFasterXmlLoadCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-load-fxml";
    }

    @Override
    public String description() {
        return "загружает fasterXml.xml файл в базу.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().loadFasterXml(session);
        System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
    }
}
