package ru.iteco.vetoshnikov.taskmanager.command.about;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {
    public AboutCommand() {
        super();
        setSecure(false);
    }

    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "Информация о программе.";
    }

    @Override
    public void execute()  {
        @NotNull final String buildNumber = Manifests.read("buildnumber");
        System.out.println("Номер сборки: " + buildNumber);
    }
}
