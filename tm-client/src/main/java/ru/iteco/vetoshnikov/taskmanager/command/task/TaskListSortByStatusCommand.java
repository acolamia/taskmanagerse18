package ru.iteco.vetoshnikov.taskmanager.command.task;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@NoArgsConstructor
public final class TaskListSortByStatusCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list-sortbystatus";
    }

    @Override
    public String description() {
        return "отображает список задач, сортируя по текущему статусу.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Введите имя проекта, в котором нужнор произвести сортировку: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session.getUserId(), projectName);
        System.out.println("Список проектов сотрированный по текущему статусу:");
        @NotNull final List<TaskDTO> taskList = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllByProjectTask(session.getUserId(),projectId);
        @NotNull final List<TaskDTO> statusListPlanned = new ArrayList<>();
        @NotNull final List<TaskDTO> statusListInprogress = new ArrayList<>();
        @NotNull final List<TaskDTO> statusListComplete = new ArrayList<>();
        for (@NotNull final TaskDTO getTask : taskList) {
            @NotNull final boolean isStatusPlanned = getTask.getStatusType().equals(StatusType.PLANNED);
            @NotNull final boolean isStatusInprogress = getTask.getStatusType().equals(StatusType.INPROGRESS);
            @NotNull final boolean isStatusComplete = getTask.getStatusType().equals(StatusType.COMPLETE);
            if (isStatusPlanned) {
                statusListPlanned.add(getTask);
            }
            if (isStatusInprogress) {
                statusListInprogress.add(getTask);
            }
            if (isStatusComplete) {
                statusListComplete.add(getTask);
            }
        }
        @NotNull final List<TaskDTO> allList = new LinkedList<>();
        allList.addAll(statusListPlanned);
        allList.addAll(statusListInprogress);
        allList.addAll(statusListComplete);
        for (@NotNull final TaskDTO getTask : allList) {
            System.out.println(getTask.getName() + " - " + getTask.getStatusType());
        }
    }
}