package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.UserDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

@NoArgsConstructor
public final class UserSetPasswordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "set-password";
    }

    @Override
    public String description() {
        return "Изменение пароля.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите логин пользователя для которого хотите изменить пароль: ");
        @Nullable final String login = service.getScanner().nextLine();
        System.out.print("Введите новый пароль: ");
        @Nullable final String newPassword = service.getScanner().nextLine();
        @Nullable final UserDTO user = serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(login);
        user.setPassword(HashUtil.getHash(newPassword));
        serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(user);

    }
}