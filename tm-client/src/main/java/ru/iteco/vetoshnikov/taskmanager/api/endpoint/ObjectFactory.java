
package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.iteco.vetoshnikov.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPort_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "getPort");
    private final static QName _GetPortResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "getPortResponse");
    private final static QName _GetSession_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "getSession");
    private final static QName _GetSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "getSessionResponse");
    private final static QName _GetUrl_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "getUrl");
    private final static QName _GetUrlResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "getUrlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.iteco.vetoshnikov.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPort }
     * 
     */
    public GetPort createGetPort() {
        return new GetPort();
    }

    /**
     * Create an instance of {@link GetPortResponse }
     * 
     */
    public GetPortResponse createGetPortResponse() {
        return new GetPortResponse();
    }

    /**
     * Create an instance of {@link GetSession }
     * 
     */
    public GetSession createGetSession() {
        return new GetSession();
    }

    /**
     * Create an instance of {@link GetSessionResponse }
     * 
     */
    public GetSessionResponse createGetSessionResponse() {
        return new GetSessionResponse();
    }

    /**
     * Create an instance of {@link GetUrl }
     * 
     */
    public GetUrl createGetUrl() {
        return new GetUrl();
    }

    /**
     * Create an instance of {@link GetUrlResponse }
     * 
     */
    public GetUrlResponse createGetUrlResponse() {
        return new GetUrlResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPort }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "getPort")
    public JAXBElement<GetPort> createGetPort(GetPort value) {
        return new JAXBElement<GetPort>(_GetPort_QNAME, GetPort.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPortResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "getPortResponse")
    public JAXBElement<GetPortResponse> createGetPortResponse(GetPortResponse value) {
        return new JAXBElement<GetPortResponse>(_GetPortResponse_QNAME, GetPortResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "getSession")
    public JAXBElement<GetSession> createGetSession(GetSession value) {
        return new JAXBElement<GetSession>(_GetSession_QNAME, GetSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "getSessionResponse")
    public JAXBElement<GetSessionResponse> createGetSessionResponse(GetSessionResponse value) {
        return new JAXBElement<GetSessionResponse>(_GetSessionResponse_QNAME, GetSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "getUrl")
    public JAXBElement<GetUrl> createGetUrl(GetUrl value) {
        return new JAXBElement<GetUrl>(_GetUrl_QNAME, GetUrl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "getUrlResponse")
    public JAXBElement<GetUrlResponse> createGetUrlResponse(GetUrlResponse value) {
        return new JAXBElement<GetUrlResponse>(_GetUrlResponse_QNAME, GetUrlResponse.class, null, value);
    }

}
