package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.iteco.vetoshnikov.taskmanager.api.customApi.ISessionStatusService;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;

@Getter
@Setter
@NoArgsConstructor
@Service
public class SessionStatusService implements ISessionStatusService {
    @Nullable
    private SessionDTO session = null;
}
