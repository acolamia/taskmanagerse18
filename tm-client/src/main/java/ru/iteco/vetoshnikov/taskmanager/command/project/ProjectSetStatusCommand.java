package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

@NoArgsConstructor
public final class ProjectSetStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "project-change-status";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет статус проекта.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Введите имя проекта для которого нужно изменить статус: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        System.out.println("Введите статус проекта из списка:");
        for (@Nullable StatusType statusType : StatusType.values()) {
            System.out.println(statusType);
        }
        @Nullable final String newSetStatus = service.getScanner().nextLine();
        @Nullable final ProjectDTO project = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findOneProject(session.getUserId(), projectName);
        if (newSetStatus.equalsIgnoreCase("Запланировано")) {
            project.setStatusType(StatusType.PLANNED.getDisplayName());
            serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(project);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("В процессе")) {
            project.setStatusType(StatusType.INPROGRESS.getDisplayName());
            serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(project);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("Выполнено")) {
            project.setStatusType(StatusType.COMPLETE.getDisplayName());
            serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(project);
            return;
        }
    }
}
