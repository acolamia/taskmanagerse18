package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "удаляет задачу в веденном вами проекте.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Из какого проекта удалить данную задачу? : ");
        @Nullable final String projectName = service.getScanner().nextLine();
        System.out.print("Введите имя задачи которую хотите удалить: ");
        @Nullable final String taskName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session.getUserId(), projectName);
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().removeTask(session.getUserId(), projectId, taskName);
    }
}
