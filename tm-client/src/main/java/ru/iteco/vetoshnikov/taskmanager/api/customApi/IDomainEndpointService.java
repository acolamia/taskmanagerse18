package ru.iteco.vetoshnikov.taskmanager.api.customApi;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IDomainEndpoint;

import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceFeature;

public interface IDomainEndpointService {
    @WebEndpoint(name = "DomainEndpointPort")
    IDomainEndpoint getDomainEndpointPort();

    @WebEndpoint(name = "DomainEndpointPort")
    IDomainEndpoint getDomainEndpointPort(WebServiceFeature... features);
}
