package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class ProjectListSortByStatusCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list-sortbystatus";
    }

    @Override
    public String description() {
        return "отображает список проектов, сортируя по текущему статусу.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.println("Список проектов сотрированный по текущему статусу:");
        @NotNull final List<ProjectDTO> projectList = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllByUserProject(session.getUserId());
        @NotNull final List<ProjectDTO> statusListPlanned = new ArrayList<>();
        @NotNull final List<ProjectDTO> statusListInprogress = new ArrayList<>();
        @NotNull final List<ProjectDTO> statusListComplete = new ArrayList<>();
        for (@NotNull final ProjectDTO getProject : projectList) {
            @NotNull final boolean isStatusPlanned = getProject.getStatusType().equals(StatusType.PLANNED);
            @NotNull final boolean isStatusInprogress = getProject.getStatusType().equals(StatusType.INPROGRESS);
            @NotNull final boolean isStatusComplete = getProject.getStatusType().equals(StatusType.COMPLETE);
            if (isStatusPlanned) {
                statusListPlanned.add(getProject);
            }
            if (isStatusInprogress) {
                statusListInprogress.add(getProject);
            }
            if (isStatusComplete) {
                statusListComplete.add(getProject);
            }
        }
        @NotNull final List<ProjectDTO> allList = new LinkedList<>();
        allList.addAll(statusListPlanned);
        allList.addAll(statusListInprogress);
        allList.addAll(statusListComplete);
        for (@NotNull final ProjectDTO getProject : allList) {
            System.out.println(getProject.getName() + " - " + getProject.getStatusType());
        }
    }
}
