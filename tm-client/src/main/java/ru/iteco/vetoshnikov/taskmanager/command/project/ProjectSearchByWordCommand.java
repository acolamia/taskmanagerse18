package ru.iteco.vetoshnikov.taskmanager.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public class ProjectSearchByWordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-search";
    }

    @Override
    public String description() {
        return "поиск по названию проекта.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        @Nullable final String userId = session.getUserId();
        if (userId == null) return;
        System.out.println("Введите название проекта:");
        @Nullable final String word = service.getScanner().nextLine();
        @Nullable final List<ProjectDTO> projectList = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllByUserProject(session.getUserId());
        for (@Nullable final ProjectDTO project : projectList) {
            if (project == null) continue;
            if (word.equalsIgnoreCase(project.getName())) System.out.println(project.getName());
        }
    }
}
