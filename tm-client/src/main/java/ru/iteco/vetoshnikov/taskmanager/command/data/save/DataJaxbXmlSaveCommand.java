package ru.iteco.vetoshnikov.taskmanager.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataJaxbXmlSaveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-save-jaxbx";
    }

    @Override
    public String description() {
        return "сохраняет в базу jaxbXml.xml.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().saveJaxbXml(session);
        System.out.println("Выгрузка базы завершена");
    }
}
