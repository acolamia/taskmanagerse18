package ru.iteco.vetoshnikov.taskmanager.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;


@NoArgsConstructor
public class DataFasterJsonLoadCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-load-fjson";
    }

    @Override
    public String description() {
        return "загружает *.Json файл в базу.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().loadFasterJson(session);
        System.out.println("Загрузка в базу завершена, необходимо перезайти в учетную запись.");
    }
}
