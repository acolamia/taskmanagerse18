package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.UserDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {
    @Override
    public String command() {
        return "reg-newuser";
    }

    @Override
    public String description() {
        return "Регистрация нового пользователя.";
    }

    @Override
    public void execute() {
        final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите логин для нового пользователя: ");
        @Nullable final String userLogin = service.getScanner().nextLine();
        System.out.print("Введите пароль для нового пользователя: ");
        @Nullable final String userPassword = service.getScanner().nextLine();
        System.out.print("Новый пользователь получит права администратора? Введите любой из вариантов: " + "да/yes/д/y"
                + " если получит ИЛИ просто нажмите Enter, если нужны ограниченные права:");
        @NotNull final String tempUserRole = service.getScanner().nextLine();
        @Nullable boolean yes = tempUserRole.equalsIgnoreCase("ДА") || tempUserRole.equalsIgnoreCase("YES");
        @Nullable boolean y = tempUserRole.equalsIgnoreCase("Д") || tempUserRole.equalsIgnoreCase("Y");
        if (yes || y) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin(userLogin);
            user.setPassword(HashUtil.getHash(userPassword));
            user.setRole(RoleType.ADMINISTRATOR.getDisplayName());
            user.setName(userLogin);
            serviceLocator.getUserEndpointService().getUserEndpointPort().createUserUser(user);
            return;
        }
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(userLogin);
        user.setPassword(HashUtil.getHash(userPassword));
        user.setRole(RoleType.USER.getDisplayName());
        user.setName(userLogin);
        serviceLocator.getUserEndpointService().getUserEndpointPort().createUserUser(user);
        return;
    }
}
