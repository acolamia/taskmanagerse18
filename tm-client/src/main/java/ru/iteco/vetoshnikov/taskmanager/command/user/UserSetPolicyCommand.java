package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.UserDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;

@NoArgsConstructor
public final class UserSetPolicyCommand extends AbstractCommand {
    @Override
    public String command() {
        return "set-role";
    }

    @Override
    public String description() {
        return "Изменение прав доступа пользователя.";
    }

    @Override
    public void execute()  {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите логин пользователя для которого хотите изменить права: ");
        @Nullable final String login = service.getScanner().nextLine();
        @Nullable final UserDTO user = serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(login);
        System.out.print("Введите ДА, если пользователю нужно дать права администратора: ");
        @NotNull final String tempUserRole = service.getScanner().nextLine();
        if (tempUserRole.equalsIgnoreCase("ДА")) {
            user.setRole(RoleType.ADMINISTRATOR.getDisplayName());
            serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(user);
            return;
        }
        user.setRole(RoleType.USER.getDisplayName());
        serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(user);
        return;
    }
}
