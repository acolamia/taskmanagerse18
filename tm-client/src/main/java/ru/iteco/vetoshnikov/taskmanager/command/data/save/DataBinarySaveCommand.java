package ru.iteco.vetoshnikov.taskmanager.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataBinarySaveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-save-bin";
    }

    @Override
    public String description() {
        return "сохраняет базу в *.bin файл.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().saveBinary(session);
        System.out.println("Выгрузка базы завершена");
    }
}
