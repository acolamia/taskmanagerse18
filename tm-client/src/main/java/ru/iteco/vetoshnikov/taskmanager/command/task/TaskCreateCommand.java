package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "создается задача в веденном вами проекте.";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите имя новой задачи: ");
        @Nullable final String taskName = service.getScanner().nextLine();
        System.out.print("В какой проект положить данную задачу? : ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session.getUserId(), projectName);
        @Nullable final TaskDTO task = new TaskDTO();
        task.setName(taskName);
        task.setUserId(session.getUserId());
        task.setProjectId(projectId);
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().createTaskTask(task);
    }
}
