package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.UserDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Component
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {
    @Autowired
    private IUserService userService;
    @Autowired
    private ConvertEndtityAndDTOUtil convertUtil;

    @Override
    @WebMethod
    public void createUserUser(
            @WebParam(name = "userObject") @Nullable final UserDTO userObject
    ) {
        User user = convertUtil.convertDTOToUser(userObject);
        userService.createUser(user);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "userObject") @Nullable final UserDTO userObject
    ) {
        User user = convertUtil.convertDTOToUser(userObject);
        userService.merge(user);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "userName") @Nullable final String userName
    ) {
        User user = userService.findOne(userName);
        System.out.println(user.getId());
        userService.remove(user.getId());
    }

    @Override
    @WebMethod
    public void clearUser(
    ) {
        userService.clear();
    }

    @Override
    @WebMethod
    public UserDTO findOneUser(
            @WebParam(name = "userName") @Nullable final String userName
    ) {
        return convertUtil.convertUserToDTO(userService.findOne(userName));
    }

    @Override
    @WebMethod
    public List<UserDTO> findAllUser(
    ) {
        return convertUtil.convertUserToDTOList(userService.findAll());
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "domainObject") @Nullable final DomainDTO domainObject
    ) {
        userService.load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdUserUser(
            @WebParam(name = "userName") @Nullable final String userName
    ) {
        return userService.getIdUser(userName);
    }
}
