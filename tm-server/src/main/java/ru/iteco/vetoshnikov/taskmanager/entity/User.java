package ru.iteco.vetoshnikov.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {
    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    List<Session> sessionList ;
    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    List<Project> projectList ;
    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    List<Task> taskList ;

    @Nullable
    @Column(name = "login",unique = true)
    private String login = null;
    @Nullable
    @Column(name = "password")
    private String password = null;
    @NotNull
    @Column(name = "role")
    private String role = RoleType.USER.getDisplayName();
    @Nullable
    @Column(name = "name")
    private String name = "Имя не задано";
    @Nullable
    @Column(name = "description")
    private String description = null;
    @Nullable
    @Column(name = "beginDate")
    private Date beginDate = null;
    @Nullable
    @Column(name = "endDate")
    private Date endDate = null;
}
