package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

public interface ISessionService {
    User checkPassword(
            @Nullable final String userLogin,
            @Nullable final String userPassword
    );

    Session createSession(
            @Nullable final Session session
    );

    void removeSession(
            @Nullable final Session session
    );

    Session findSession(
            @Nullable final Session session
    );

    String getPort();

    String getUrl();
}
