package ru.iteco.vetoshnikov.taskmanager.enumerate;

public enum RoleType {
    ADMINISTRATOR("Администратор"),

    USER("Пользователь");

    private final String displayName;

    RoleType(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
