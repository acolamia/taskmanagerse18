package ru.iteco.vetoshnikov.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.UserDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertEndtityAndDTOUtil {

    //**********************************************
    // Далее идут методы конвертации СУЩНОСТЕЙ в ДТО
    //**********************************************

    public ProjectDTO convertProjectToDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setCreateDate(project.getCreateDate());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setBeginDate(project.getBeginDate());
        projectDTO.setEndDate(project.getEndDate());
        projectDTO.setStatusType(project.getStatusType());
        projectDTO.setUserId(project.getUser().getId());
        return projectDTO;
    }

    public List<ProjectDTO> convertProjectToDTOList(@Nullable final List<Project> projectList) {
        if (projectList == null) return null;
        @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projectList) {
            projectDTOList.add(convertProjectToDTO(project));
        }
        return projectDTOList;
    }

    public TaskDTO convertTaskToDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setCreateDate(task.getCreateDate());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setBeginDate(task.getBeginDate());
        taskDTO.setEndDate(task.getEndDate());
        taskDTO.setStatusType(task.getStatusType());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    public List<TaskDTO> convertTaskToDTOList(@Nullable final List<Task> taskList) {
        if (taskList == null) return null;
        @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : taskList) {
            taskDTOList.add(convertTaskToDTO(task));
        }
        return taskDTOList;
    }

    public UserDTO convertUserToDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setCreateDate(user.getCreateDate());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    public List<UserDTO> convertUserToDTOList(@Nullable final List<User> userList) {
        if (userList == null) return null;
        @NotNull final List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : userList) {
            userDTOList.add(convertUserToDTO(user));
        }
        return userDTOList;
    }

    //*********************************************
    // Далее идут методы конвертации ДТО в СУЩНОСТИ
    //*********************************************

    public Project convertDTOToProject(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setCreateDate(projectDTO.getCreateDate());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setBeginDate(projectDTO.getBeginDate());
        project.setEndDate(projectDTO.getEndDate());
        project.setStatusType(projectDTO.getStatusType());
        @NotNull final User user = new User();
        user.setId(projectDTO.getUserId());
        project.setUser(user);
        return project;
    }

    public List<Project> convertDTOToProjectList(@Nullable final List<ProjectDTO> projectDTOList) {
        if (projectDTOList == null) return null;
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : projectDTOList) {
            projectList.add(convertDTOToProject(projectDTO));
        }
        return projectList;
    }

    public Task convertDTOToTask(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setCreateDate(taskDTO.getCreateDate());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setBeginDate(taskDTO.getBeginDate());
        task.setEndDate(taskDTO.getEndDate());
        task.setStatusType(taskDTO.getStatusType());
        @NotNull final User user = new User();
        user.setId(taskDTO.getUserId());
        task.setUser(user);
        @NotNull final Project project = new Project();
        project.setId(taskDTO.getProjectId());
        task.setProject(project);
        return task;
    }

    public List<Task> convertDTOToTaskList(@Nullable final List<TaskDTO> taskDTOList) {
        if (taskDTOList == null) return null;
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (TaskDTO taskDTO : taskDTOList) {
            taskList.add(convertDTOToTask(taskDTO));
        }
        return taskList;
    }

    public User convertDTOToUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setCreateDate(userDTO.getCreateDate());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        return user;
    }

    public List<User> convertDTOToUserList(@Nullable final List<UserDTO> userDTOList) {
        if (userDTOList == null) return null;
        @NotNull final List<User> userList = new ArrayList<>();
        for (UserDTO userDTO : userDTOList) {
            userList.add(convertDTOToUser(userDTO));
        }
        return userList;
    }

    //*******************************************
    // Далее идут методы конвертация СЕССИИ в ДТО
    //*******************************************

    public SessionDTO convertSessionToDTO(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setSignature(session.getSignature());
        return sessionDTO;
    }
}
