package ru.iteco.vetoshnikov.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity {
    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    List<Task> taskList ;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    @Column(name = "name")
    private String name = null;
    @Nullable
    @Column(name = "description")
    private String description = null;
    @Nullable
    @Column(name = "beginDate")
    private Date beginDate = null;
    @Nullable
    @Column(name = "endDate")
    private Date endDate = null;
    @NotNull
    @Column(name = "statusType")
    private String statusType = StatusType.PLANNED.getDisplayName();
}
