package ru.iteco.vetoshnikov.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NoArgsConstructor
@Table(name = "app_task")
public class Task extends AbstractEntity {
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Nullable
    @Column(name = "name")
    private String name = null;
    @Nullable
    @Column(name = "description")
    private String description = null;
    @Nullable
    @Column(name = "beginDate")
    private Date beginDate = null;
    @Nullable
    @Column(name = "endDate")
    private Date endDate = null;
    @NotNull
    @Column(name = "statusType")
    private String statusType = StatusType.PLANNED.getDisplayName();
}
