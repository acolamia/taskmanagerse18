package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;

public interface IDomainService {
    void load(@NotNull final DomainDTO domain);

    void save(@NotNull final DomainDTO domain);
}
