package ru.iteco.vetoshnikov.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO {
    @Nullable
    private String id = UUID.randomUUID().toString();
    @Nullable
    private Long timestamp = new Date().getTime();
    @Nullable
    private String userId;
    @Nullable
    private String signature;
}
