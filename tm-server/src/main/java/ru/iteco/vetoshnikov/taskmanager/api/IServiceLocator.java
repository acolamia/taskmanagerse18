package ru.iteco.vetoshnikov.taskmanager.api;

import ru.iteco.vetoshnikov.taskmanager.service.ProjectService;
import ru.iteco.vetoshnikov.taskmanager.service.SessionService;
import ru.iteco.vetoshnikov.taskmanager.service.TaskService;
import ru.iteco.vetoshnikov.taskmanager.service.UserService;

public interface IServiceLocator {
    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    SessionService getSessionService();
}
