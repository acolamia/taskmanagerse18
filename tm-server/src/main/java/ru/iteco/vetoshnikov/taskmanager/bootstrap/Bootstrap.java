package ru.iteco.vetoshnikov.taskmanager.bootstrap;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.api.service.*;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;

@Component
public class Bootstrap {
    @Nullable
    @Autowired
    private IDomainEndpoint domainEndpoint;
    @Nullable
    @Autowired
    private ISessionEndpoint sessionEndpoint;
    @Nullable
    @Autowired
    private IProjectEndpoint projectEndpoint;
    @Nullable
    @Autowired
    private ITaskEndpoint taskEndpoint;
    @Nullable
    @Autowired
    private IUserEndpoint userEndpoint;
    @Nullable
    @Autowired
    private IUserService userService;
    @Nullable
    @Autowired
    private IProjectService projectService;
    @Nullable
    @Autowired
    private ITaskService taskService;
    @Nullable
    @Autowired
    private ISessionService sessionService;
    @Nullable
    @Autowired
    private IDomainService domainService;

    private static String port = "8080";
    private static String url = "localhost";

    public void init() {
        try {
            final String setPort = System.getProperty("server.port");
            if (setPort != null) {
                port = setPort;
            }
            setEndpoint();
//            addUserAndAdminUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEndpoint() {
        Endpoint.publish("http://" + url + ":" + port + "/DomainWebService?wsdl", domainEndpoint);
        Endpoint.publish("http://" + url + ":" + port + "/ProjectWebService?wsdl", projectEndpoint);
        Endpoint.publish("http://" + url + ":" + port + "/TaskWebService?wsdl", taskEndpoint);
        Endpoint.publish("http://" + url + ":" + port + "/UserWebService?wsdl", userEndpoint);
        Endpoint.publish("http://" + url + ":" + port + "/SessionWebService?wsdl", sessionEndpoint);
    }

    public static String getPort() {
        return port;
    }

    public static String getUrl(){
        return url;
    }

//    private void addUserAndAdminUser() {
//        User guest = new User();
//        guest.setLogin("guest");
//        guest.setPassword(HashUtil.getHash("guest"));
//        guest.setRole(RoleType.USER.getDisplayName());
//        guest.setName("guest");
//        userService.createUser(guest);
//        User admin = new User();
//        admin.setLogin("admin");
//        admin.setPassword(HashUtil.getHash("admin"));
//        admin.setRole(RoleType.ADMINISTRATOR.getDisplayName());
//        admin.setName("admin");
//        userService.createUser(admin);
//    }
}
