package ru.iteco.vetoshnikov.taskmanager.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractDTO implements Serializable {
    @NotNull
    private String id = UUID.randomUUID().toString();
    @Nullable
    private String name="Имя не задано";
    @Nullable
    private String description=null;
    @Nullable
    private Date beginDate=null;
    @Nullable
    private Date endDate=null;
    @Nullable
    private Date createDate=new Date();
}
