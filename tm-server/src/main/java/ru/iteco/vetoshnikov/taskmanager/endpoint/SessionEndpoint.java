package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;
import ru.iteco.vetoshnikov.taskmanager.dto.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Setter
@NoArgsConstructor
@Component
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private ConvertEndtityAndDTOUtil convertUtil;

    @Override
    @WebMethod
    public SessionDTO getSession(
            @WebParam(name = "userLogin") @Nullable final String userLogin,
            @WebParam(name = "userPass") @Nullable final String userPass) {
        @Nullable final User user = sessionService.checkPassword(userLogin, userPass);
        if (user == null) {
            return null;
        }
        @NotNull Session session = new Session();
        session.setUser(user);
        String signature = SignatureUtil.sign(session);
        session.setSignature(signature);
        sessionService.createSession(session);
        return convertUtil.convertSessionToDTO(session);
    }

    @Override
    public String getPort() {
        return sessionService.getPort();
    }

    @Override
    public String getUrl() {
        return sessionService.getUrl();
    }
}
