package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Component
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    @Autowired
    private ITaskService taskService;
    @Autowired
    private ConvertEndtityAndDTOUtil convertUtil;

    @Override
    @WebMethod
    public void createTaskTask(
            @WebParam(name = "taskObject") @Nullable final TaskDTO taskObject
    ) {
        Task task = convertUtil.convertDTOToTask(taskObject);
        taskService.createTask(task);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "taskObject") @Nullable final TaskDTO taskObject
    ) {
        Task task = convertUtil.convertDTOToTask(taskObject);
        taskService.merge(task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        taskService.remove(userId, projectId, taskName);
    }

    @Override
    @WebMethod
    public void clearTask(
    ) {
        taskService.clear();
    }

    @Override
    @WebMethod
    public TaskDTO findOneTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        return convertUtil.convertTaskToDTO(taskService.findOne(userId, projectId, taskName));
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllTask(
    ) {
        return convertUtil.convertTaskToDTOList(taskService.findAll());
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllByProjectTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        return convertUtil.convertTaskToDTOList(taskService.findAllByProject(userId, projectId));
    }

    @Override
    @WebMethod
    public void removeAllByProjectTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        taskService.removeAllByProject(userId, projectId);
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "domainObject") @Nullable final DomainDTO domainObject
    ) {
        taskService.load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdTaskTask(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        return taskService.getIdTask(userId, projectId, taskName);
    }
}
