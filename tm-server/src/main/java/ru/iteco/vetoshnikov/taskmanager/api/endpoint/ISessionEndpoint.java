package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @WebMethod
    SessionDTO getSession(
            @WebParam(name = "userLogin") @Nullable String userLogin,
            @WebParam(name = "userPass") @Nullable String userPass
    );

    @WebMethod
    String getPort();

    @WebMethod
    String getUrl();
}
