package ru.iteco.vetoshnikov.taskmanager.dto;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractDTO {
    @NotNull
    private String login=null;
    @NotNull
    private String password=null;
    @NotNull
    private String role = RoleType.USER.getDisplayName();

    public UserDTO(@NotNull final String login, @NotNull final String password, @NotNull final String role) {
        this.login = login;
        this.password = HashUtil.getHash(password);
        this.role = role;
    }
}
