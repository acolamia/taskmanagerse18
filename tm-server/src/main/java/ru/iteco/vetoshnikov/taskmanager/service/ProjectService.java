package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import java.util.List;

@Component
@Transactional
public class ProjectService implements IProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public void createProject(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    public void merge(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) return;
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            projectRepository.save(project);
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return;
        List<Project> projectList = projectRepository.findAllByUserId(userId);
        for (Project project : projectList) {
            if (projectName.equals(project.getName())) {
                projectRepository.delete(project);
            }
        }
    }

    @Override
    public void removeAllByUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return null;
        List<Project> projectList = projectRepository.findAllByUserId(userId);
        for (Project project : projectList) {
            if (projectName.equals(project.getName())) {
                return project;
            }
        }
        return null;
    }

    @Override
    public @Nullable
    final List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @Nullable
    final List<Project> findAllByUser(@Nullable final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void load(@Nullable final DomainDTO domain) {
        if (domain == null) return;
        projectRepository.deleteAll();
        @Nullable final List<Project> projectList = new ConvertEndtityAndDTOUtil()
                .convertDTOToProjectList(domain.getProjectList());
        projectRepository.saveAll(projectList);
    }

    @Override
    public String getIdProject(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty() || name == null || name.isEmpty()) return null;
        List<Project> projectList = projectRepository.findAllByUserId(userId);
        for (Project project : projectList) {
            if (name.equals(project.getName())) {
                return project.getId();
            }
        }
        return null;
    }
}
