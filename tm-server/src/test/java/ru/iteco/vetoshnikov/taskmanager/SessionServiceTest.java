package ru.iteco.vetoshnikov.taskmanager;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.config.SpringConfigTest;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class)
public class SessionServiceTest extends Assert {
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IUserService userService;

    private User user;

    private Session session;

    @Before
    public void setUp() throws Exception {
        user = new User();
        userService.createUser(user);
        session = new Session();
        session.setUser(user);
        String signature = SignatureUtil.sign(session);
        session.setSignature(signature);
    }

    @After
    public void tearDown() throws Exception {
        sessionService.removeSession(session);
        userService.remove(user.getId());
    }

    @Test
    public void createSession() {
        Session getSession = sessionService.createSession(session);
        assertNotNull(getSession.getId());
        assertNotNull(getSession.getSignature());
        assertNotNull(getSession.getTimestamp());
        assertNotNull(getSession.getUser());
    }

    @Test
    public void removeSession() {
        Session getSession = sessionService.createSession(session);
        assertNotNull(getSession);
    }
}