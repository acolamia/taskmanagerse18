package ru.iteco.vetoshnikov.taskmanager;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.config.SpringConfigTest;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class)
public class TaskServiceTest extends Assert {

    @Autowired
    private IUserService userService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    private User testUser;

    private Project testProject;

    private Task testTask;
    private Task testTask2;

    @Before
    public void setUp() throws Exception {
        testUser = new User();
        testUser.setLogin("test");
        testUser.setPassword("test");
        testUser.setName("test");
        testUser.setRole(RoleType.USER.getDisplayName());
        userService.createUser(testUser);

        testProject = new Project();
        testProject.setName("testProject");
        testProject.setUser(testUser);
        projectService.createProject(testProject);

        testTask = new Task();
        testTask.setUser(testUser);
        testTask.setProject(testProject);
        testTask.setName("testTask");
        taskService.createTask(testTask);

        testTask2 = new Task();
        testTask2.setUser(testUser);
        testTask2.setProject(testProject);
        testTask2.setName("testTask");
        taskService.createTask(testTask2);
    }

    @After
    public void tearDown() throws Exception {
        taskService.removeAllByProject(testUser.getId(), testProject.getId());
        projectService.removeAllByUser(testUser.getId());
        userService.remove(testUser.getId());
    }

    @Test
    public void createTask() {
        Task getTask = taskService.findOne(testUser.getId(), testProject.getId(), testTask.getName());
        assertNotNull(testTask);
        assertNotNull(getTask);
        assertEquals(testTask.getName(), getTask.getName());
    }

    @Test
    public void merge() {
        testTask.setName("testTaskRename");
        testTask2.setName("testTask2Rename");
        List<Task> taskList = new ArrayList<>();
        taskList.add(testTask);
        taskList.add(testTask2);
        taskService.merge(taskList);
        List<Task> getTaskList = taskService.findAllByProject(testUser.getId(), testProject.getId());
        for (Task task : getTaskList) {
            assertNotNull(task);
            if (task.getId().equals(testTask.getId())) assertEquals(task.getName(), testTask.getName());
            if (task.getId().equals(testTask2.getId())) assertEquals(task.getName(), testTask2.getName());
        }
    }

    @Test
    public void remove() {
        taskService.remove(testUser.getId(), testProject.getId(), testTask.getName());
        Task task = taskService.findOne(testUser.getId(), testProject.getId(), testTask.getName());
        assertNull(task);
    }

    @Test
    public void removeAllByProject() {
        taskService.removeAllByProject(testUser.getId(), testProject.getId());
        List<Task> taskList = taskService.findAllByProject(testUser.getId(), testProject.getId());
        for (Task task : taskList) {
            assertNull(task);
        }
    }

    @Test
    public void clear() {
        taskService.clear();
        List<Task> taskList = taskService.findAll();
        for (Task task : taskList) {
            assertNull(task);
        }
    }

    @Test
    public void findOne() {
        Task task = taskService.findOne(testUser.getId(), testProject.getId(), testTask.getName());
        assertNotNull(task);
        assertEquals(task.getName(), testTask.getName());
    }

    @Test
    public void findAll() {
        List<Task> taskList = null;
        taskList = taskService.findAll();
        assertFalse(taskList.isEmpty());
    }

    @Test
    public void findAllByProject() {
        List<Task> taskList = null;
        taskList = taskService.findAllByProject(testUser.getId(),testProject.getId());
        assertFalse(taskList.isEmpty());
    }

    @Test
    public void getIdTask() {
        Task task = taskService.findOne(testUser.getId(), testProject.getId(), testTask.getName());
        assertNotNull(task);
        assertEquals(task.getId(), testTask.getId());
    }
}