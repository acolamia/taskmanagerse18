package ru.iteco.vetoshnikov.taskmanager;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.config.SpringConfigTest;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class)
public class UserServiceTest extends Assert {

    @Autowired
    private IUserService userService;

    private User testUser;

    @Before
    public void setUp() throws Exception {
        testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPassword("testUser");
        testUser.setRole(RoleType.USER.getDisplayName());
        userService.createUser(testUser);
    }

    @After
    public void tearDown() throws Exception {
        userService.clear();
    }

    @Test
    public void createUser() {
        User getUser = userService.findOne(testUser.getLogin());
        assertNotNull(testUser);
        assertNotNull(getUser);
        assertEquals(getUser.getLogin(), testUser.getLogin());
    }

    @Test
    public void merge() {
        testUser.setLogin("testUser2");
        List<User> userList = new ArrayList<>();
        userList.add(testUser);
        userService.merge(userList);
        List<User> getUserList = userService.findAll();
        for (User user : getUserList) {
            assertNotNull(user);
            if (user.getId().equals(testUser.getId())) assertEquals(user.getLogin(), testUser.getLogin());
        }
    }

    @Test
    public void remove() {
        userService.remove(testUser.getId());
        User getUser = userService.findOne(testUser.getLogin());
        assertNull(getUser);
    }

    @Test
    public void findOne() {
        User getUser = userService.findOne(testUser.getLogin());
        assertNotNull(getUser);
        assertEquals(testUser.getLogin(), getUser.getLogin());
    }

    @Test
    public void findAll() {
        List<User> userList = null;
        userList = userService.findAll();
        assertFalse(userList.isEmpty());
    }

    @Test
    public void getIdUser() {
        User getUser = userService.findOne(testUser.getLogin());
        assertNotNull(getUser);
        assertEquals(getUser.getId(), testUser.getId());
    }

    @Test
    public void clear() {
        userService.clear();
        List<User> userList = null;
        userList = userService.findAll();
        assertTrue(userList.isEmpty());
    }
}